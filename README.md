proftpd-mod_counter
===================

Status
------
[![Build Status](https://travis-ci.org/Castaglia/proftpd-mod_counter.svg?branch=master)](https://travis-ci.org/Castaglia/proftpd-mod_counter)
[![License](https://img.shields.io/badge/license-GPL-brightgreen.svg)](https://img.shields.io/badge/license-GPL-brightgreen.svg)

Synopsis
--------
The `mod_counter` module for ProFTPD provides limits on the number
of concurrent uploads/downloads for files.

For further module documentation, see [mod_counter.html](https://htmlpreview.github.io/?https://github.com/Castaglia/proftpd-mod_counter/blob/master/mod_counter.html).
